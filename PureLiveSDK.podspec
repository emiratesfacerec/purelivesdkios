Pod::Spec.new do |s|
  s.name = 'PureLiveSDK'
  s.version = '7.1.11.1'
  s.summary = 'PureLiveSDKiOS'
  s.homepage = 'https://bitbucket.org/emiratesfacerec/purelivesdkios'
  s.authors = { 'PureLive' => 'info@aicenter.ae' }
  s.source = { :git => 'https://bitbucket.org/emiratesfacerec/purelivesdkios.git' , :tag => '7.1.11', :submodules => true }
  s.swift_version = ['5.4','5.5']

  s.vendored_frameworks = 'PureLiveSDK.xcframework'
  s.ios.deployment_target  = '11.0'
  #s.resources = 'PureLiveSDKResources.bundle'

  s.static_framework = true
  #s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  #s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
 # s.pod_target_xcconfig = { 'ONLY_ACTIVE_ARCH' => 'YES' }
  s.dependency 'TensorFlowLiteC', '2.11.0'

end
